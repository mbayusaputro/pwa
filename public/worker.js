// // Flag for enabling cache in production
// // const doCache = true;
// const cacheName = "asitaaja";
// const urlsToCache = ["./", "./public/logo.png", "./src/index.js"];

// self.addEventListener("install", async event => {
//   console.log(event);
//   const cache = await caches.open(cacheName);
//   await cache.addAll(urlsToCache);
// });

// self.addEventListener("activate", event => {
//   event.waitUntil(self.clients.claim());
// });

// self.addEventListener("fetch", event => {
//   const req = event.request;

//   if (/.*(json)$/.test(req.url)) {
//     event.respondWith(networkFirst(req));
//   } else {
//     event.respondWith(cacheFirst(req));
//   }
// });

// async function cacheFirst(req) {
//   const cache = await caches.open(cacheName);
//   const cachedResponse = await cache.match(req);
//   return cachedResponse || networkFirst(req);
// }

// async function networkFirst(req) {
//   const cache = await caches.open(cacheName);
//   try {
//     const fresh = await fetch(req);
//     cache.put(req, fresh.clone());
//     return fresh;
//   } catch (e) {
//     const cachedResponse = await cache.match(req);
//     return cachedResponse;
//   }
// }
