Test Progressive Web Apps at icube:

```
npm install
```

## DEV

```
npm run dev
```

## PROD

```
npm run build
npm start
```
