const API = "https://apidev.aeroaja.com/v1/";

function handleResponse(response) {
  return response.text().then(text => {
    const data = text && JSON.parse(text);
    return data;
  });
}

// =========== PAYMENT =========== //
export function payProduct(payload) {
  const headers = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "X-Platform-Source": "PWA",
      "X-SAI-Source": "ASITAAJA"
    },
    body: JSON.stringify(payload)
  };
  return fetch(API + `payment/midtrans`, headers).then(handleResponse);
}
// =========== PAYMENT =========== //