import React from "react";

const Card = props => {
  const { name, onPress } = props;
  return (
    <div
      onClick={onPress}
      style={{
        backgroundColor: "#fff",
        padding: 30,
        borderRadius: 8,
        boxShadow: "0.5px 1px 0.5px 0.5px #f0f0f0",
        marginBottom: 10,
        fontWeight: "bold",
      }}
    >
      {name}
    </div>
  );
};
export default Card;
