import React, { useEffect, useState } from "react";
import { useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";
import Card from "./components/Card";

const ALL_PERSONS = gql`
  {
    allPersons(first: 7) {
      id
      name
      films(first: 7) {
        title
        releaseDate
        producers
      }
    }
  }
`;

export default props => {
  const {
    loading: loadingPerson,
    error: errorPerson,
    data: allPersons,
  } = useQuery(ALL_PERSONS);

  // Props
  const {
    history: { push },
  } = props;

  // State
  const [listPerson, setPerson] = useState([]);

  // Lifecycle
  useEffect(() => {
    setPerson(allPersons);
  }, [loadingPerson]);

  const Loading = () => {
    return <h3>Loading...</h3>;
  };

  const Error = () => {
    return <h3>There's an error: {errorPerson.message}</h3>;
  };

  // Function
  const onDetail = item => {
    push("/person", item);
  };

  // Main Render
  return (
    <div>
      <h1
        style={{
          backgroundColor: "#002561",
          color: "#fff",
          padding: 20,
          marginTop: 0,
        }}
      >
        All Person
      </h1>
      <div style={{ margin: 20 }}>
        {loadingPerson
          ? Loading()
          : errorPerson
          ? Error()
          : listPerson &&
            listPerson.allPersons &&
            listPerson.allPersons.map((item, i) => {
              return (
                <Card onPress={() => onDetail(item)} key={i} name={item.name} />
              );
            })}
      </div>
    </div>
  );
};
