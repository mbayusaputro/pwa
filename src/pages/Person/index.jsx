import React from "react";
import Card from "./components/Card";

export default props => {
  // Props
  const {
    history: { goBack },
    location: { state },
  } = props;

  // Main Render
  return (
    <div>
      <div
        style={{
          backgroundColor: "#002561",
          padding: 20,
        }}
      >
        <h1 style={{ color: "#fff", margin: 0 }} onClick={() => goBack()}>
          Back
        </h1>
      </div>
      <div style={{ margin: 20 }}>
        <Card name={state.name} films={state.films} />
      </div>
    </div>
  );
};
