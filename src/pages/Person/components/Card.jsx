import React from "react";
import moment from "moment";

const Card = props => {
  // Props
  const { name, films } = props;

  // Main Render
  return (
    <div>
      <div
        style={{
          backgroundColor: "#fff",
          padding: 30,
          borderRadius: 8,
          boxShadow: "0.5px 1px 0.5px 0.5px #f0f0f0",
          marginBottom: 10,
          fontWeight: "bold",
        }}
      >
        {name}
      </div>
      {films.map((item, i) => {
        return (
          <div
            key={i}
            style={{
              backgroundColor: "#fff",
              padding: 30,
              borderRadius: 8,
              boxShadow: "0.5px 1px 0.5px 0.5px #f0f0f0",
              marginBottom: 10,
            }}
          >
            <div style={{ paddingBottom: 10 }}>Film: {item.title}</div>
            <div style={{ paddingBottom: 10 }}>
              Release Date:
              {moment(item.releaseDate).format("ddd, DD MMM YYYY")}
            </div>
            <div>Producers: {item.producers.map(item => item + ", ")}</div>
          </div>
        );
      })}
    </div>
  );
};

export default Card;
