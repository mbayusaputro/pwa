import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import ListPerson from './pages/ListPerson'
import Person from "./pages/Person/index";

const route = [
  {
    path: "/person",
    components: Person
  },
];

const Router = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={ListPerson} />
      {route.map((routes, i) => (
        <Route key={i} path={routes.path} component={routes.components} />
      ))}
    </Switch>
  </BrowserRouter>
);

export default Router;
