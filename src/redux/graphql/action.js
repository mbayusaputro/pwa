import { PERSON, LIST_PERSON, LIST_PERSON_FAILED, LIST_PERSON_SUCCESS } from "./type";

const person = "person";
const list = "list";

const requestState = (data, type) => {
  if (type === person) {
    return {
      type: PERSON,
      data
    };
  } else if (type === list) {
    return {
      type: LIST_PERSON,
      data
    };
  };
}

const successState = (data) => {
  return {
    type: LIST_PERSON_SUCCESS,
    data,
  };
}

const failedState = (message) => {
  return {
    type: LIST_PERSON_FAILED,
    message,
  };
}

export const actionSave = (dispatch, payload) => {
  dispatch(requestState(payload, contact));
};

export const actionListPerson = (dispatch, payload, callback) => {
  dispatch(requestState(list, payload));
  return getFlight(payload)
    .then(res => {
      dispatch(successState(res.data));
      callback.call(this, res);
    })
    .catch(err => {
      return dispatch(failedState(err.message));
    });
};