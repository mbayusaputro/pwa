import { LIST_PERSON, LIST_PERSON_SUCCESS, PERSON, LIST_PERSON_FAILED } from "./type";

const initialState = {
  person: null,
  allPersons: null,
  isLoading: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case PERSON:
      return {
        ...state,
        data: action.data
      };
    case LIST_PERSON:
      return {
        ...state,
        isLoading: true,
      };
    case LIST_PERSON_SUCCESS:
      return {
        ...state,
        allPersons: action.data,
        isLoading: false,
      };
    case LIST_PERSON_FAILED:
      return {
        ...state,
        allPersons: action.message,
        isLoading: false,
      };
    default:
      return state;
  }
};

export default reducer;
